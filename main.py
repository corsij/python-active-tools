# -*- coding: utf-8 -*-
"""
Created on Tue Feb  7 11:10:29 2023

@author: corsij

Script servant d'exemple pour utiliser la classe Transistor et analyser 
la stabilité et le gain d'un transistor à partir d'un fichier s2p.
"""

from transistor import Transistor

## Import S-parameters from a s2p file located in the folder "s2p_files": 
s2p_files_directory="s2p_files/"
print("Enter Sparameter filename:")
filename=input("filename?")

## Create a new object Transistor, named in this example "My_Transistor"
My_Transistor=Transistor(s2p_files_directory+filename)


## Get the S-parameters of the transistor:
Sparam=My_Transistor.s

## Print S-parameters of the transistor:
My_Transistor.plot_Sparam()

## Get the Stability Rollet factor of the transistor:
K_factor=My_Transistor.K

## Print Stability Rollet factor of the transistor:
My_Transistor.plot_rollet()

## Print Transducer Gain of the transistor:
My_Transistor.plot_gain_transducer()

## Get the Stability µ factor of the transistor:
mu_factor=My_Transistor.mu_stab

## Print Stability µ factor of the transistor:
My_Transistor.plot_mu_stab()

## Print Stability factors of the transistor:
My_Transistor.stability()

## Print Stability circles (Source and Load) of the transistor at a given frequency:
circle_number_points=100
My_Transistor.stability_circle(circle_number_points)

