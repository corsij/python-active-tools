# Python Active Tools

## Description 
Programme conçu sous Spyder version 5.4.2, Python version 3.10.9

Programme permettant l'affichage des paramètres de stabilité d'un transistor à partir des ses paramètres-S.

Utilisation de la librairie Scikit-RF pour manipuler des paramètres-S, de la librairie numpy pour manipuler des matrices.

## Add your files

```
cd existing_repo
git remote add origin https://gricad-gitlab.univ-grenoble-alpes.fr/corsij/python-active-tools.git
git branch -M main
git push -uf origin main
```
## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Roadmap and To do

### A été fait:
Les paramètres de stabilités ont été vérifiés avec ADS. 

Les circles de stabilités ont été vérifiés avec ADS. 

La condition pour déterminer la zone de stabilité (intérieur ou extérieur au cercle) a été corrigée.

Ajout d'une commande pour que l'utilisateur détermine sur la console, la fréquence à laquelle il souhaite tracer les cercles de stabilités.

Ajout d'une commande pour que l'utilisateur détermine sur la console, l'impédance de charge ZL, l'impédance de source ZS.

Affichage des différents gains, en fonction de la fréquence.

Cercles de gain

Cercles de Bruit

### A faire:

Cercles de gain

Matching