# -*- coding: utf-8 -*-
"""
Created on Tue Feb  7 11:19:12 2023

@author: corsij

Programme définissant un objet Transistor contenant :
    _ Les paramètres S
    _ Les paramètres de stabilité
    _ Le bruit
    _ Les gains
    
ce programme utilise les fonctions de rf_active_tools.py pour le caclul des gains et de la stabilité.
    
Ce programme contient également des foncitons pour tracer ces paramètres en fonction de la fréquence.

"""
from matplotlib import pyplot as plt
import numpy as np
import skrf as rf
from rf_active_tools import *

class Transistor:

    def __init__(self, filename: str):
        # Init
        self.Network=rf.Network(filename)
        self.name=self.Network.name
        self.s=self.Network.s
        self.freq=self.Network.frequency.f_scaled
        
        self.mu_stab=[]
        self.K=[]
        self.delta=[]
        
        self.U=[]
        
        self.GT=[]
        self.GA=[]
        self.G=[]
        self.GMAX=[]
        self.GS=[]
        
        # Unilateral test
        self.__set_unilateral_test()
        
        # Calcul des paramètres de stabilité
        self.__set_mu_stab() 
        self.__set_rollet()
        
        # Calcul du gain
        self.Z0=self.Network.z0[0][0]
        self.ZS=complex(input("\nSource impedance ?") or self.Z0)
        print(f"Zs = Z0 = {self.ZS} Ohm")
        self.ZL=complex(input("\nLoad impedance ?") or self.Z0)
        print(f"ZL = Z0 = {self.ZL} Ohm")
        
        self.__set_gain_transducer()
        self.__set_gain_available()
        self.__set_power_gain()
        self.__set_maximum_gain()
        self.__set_stable_gain()
        
    def __set_unilateral_test(self):
        """
            Fonction privée 
            
            Calcul les paramètres de stabilité µ
            
            Appel à la fonction mu_stab, présente dans rf_active_tools.py pour chaque 
            point de fréquence
        """    
        for i,s in enumerate(self.Network.s):
            _U, _Umin, _Umax = 10*np.log10(unilateral_test(s.flatten()))
            self.U.append(list([_Umin, _Umax])) 
        
        
    def __set_mu_stab(self):
        """
            Fonction privée 
            
            Calcul les paramètres de stabilité µ
            
            Appel à la fonction mu_stab, présente dans rf_active_tools.py pour chaque 
            point de fréquence
        """    
        for i,s in enumerate(self.Network.s):
            self.mu_stab.append(list(mu_stab(s.flatten()))) 
        #print("Mu_Stab done")

    def __set_rollet(self):
        """
            Fonction privée 
            
            Calcul le paramètre de stabilité de Rollet
            
            Appel à la fonction rollet, présente dans rf_active_tools.py pour chaque 
            point de fréquence
        """    
        for i,s in enumerate(self.Network.s):
            self.K.append(rollet(s.flatten()))
            self.delta.append(s[0][0]*s[1][1]-s[1][0]*s[0][1])
        #print("Rollet done")     
        
    def __set_gain_transducer(self):
        """
            Fonction privée 
                 
            Calcul GT(transducer Gain)
                   
            Appel à la fonction correspondante présente dans rf_active_tools.py pour chaque 
            point de fréquence
        """    
        
        gammaS: complex = z_to_s(self.ZS, self.Z0)
        gammaL: complex = z_to_s(self.ZL, self.Z0)
        
        for i,s in enumerate(self.Network.s):
            self.GT.append(gain_transducer(s.flatten(), gammaS, gammaL))
        #print("GT done")
        
    def __set_gain_available(self):
        """
            Fonction privée 
                     
            Calcul GA(Available Gain)
                       
            Appel à la fonction correspondante présente dans rf_active_tools.py pour chaque 
            point de fréquence
        """    
            
        gammaS: complex = z_to_s(self.ZS, self.Z0)
            
        for i,s in enumerate(self.Network.s):
            self.GA.append(gain_availabe(s.flatten(), gammaS))
        #print("GA done")
        
    def __set_power_gain(self):
        """
            Fonction privée 
                     
            Calcul G(Power Gain)
                       
            Appel à la fonction correspondante présente dans rf_active_tools.py pour chaque 
            point de fréquence
        """    
            
        gammaL: complex = z_to_s(self.ZL, self.Z0)
            
        for i,s in enumerate(self.Network.s):
            self.G.append(gain_operating(s.flatten(), gammaL))
        #print("G done")        
        
    def __set_maximum_gain(self):
        """
            Fonction privée 
                     
            Calcul GMax(maximum Gain) 
            
            Conjugate match
                       
            Appel à la fonction correspondante présente dans rf_active_tools.py pour chaque 
            point de fréquence
            
            Calculates the power gain of a 2-port network when load and source are matched for maximum power transfer. If the K factor is not greater than one, returns NaN.

        """    
            
        for i,s in enumerate(self.Network.s):
            self.GMAX.append(gain_maximum(s.flatten()))
        #print("GMax")           
        
    def __set_stable_gain(self):
        """
            Fonction privée 
                     
            Calcul GMax(maximum Gain) 
            
            Conjugate match
                       
            Appel à la fonction correspondante présente dans rf_active_tools.py pour chaque 
            point de fréquence
            
            Calculates the maximum stable power gain of a conditionaly stable 2-port network.

        """    
            
        for i,s in enumerate(self.Network.s):
            self.GS.append(gain_maximum_stable(s.flatten()))
        #print("GMax")                

    def __RL_CL_circle(self, i):
        """
            Private function
            
            Compute the Center CL and the radius RL of the load stability circle
        """
        CL_num=np.conj(self.s[i][1][1]-self.delta[i]*np.conj(self.s[i][0][0]))   
        CL_den=np.absolute(self.s[i][1][1])**2-np.absolute(self.delta[i])**2 
        CL=(CL_num/CL_den)
        del CL_num
        del CL_den
        
        RL_num=np.absolute(self.s[i][0][1]*self.s[i][1][0])
        RL_den=np.absolute(self.s[i][1][1])**2-np.absolute(self.delta[i])**2
        RL=RL_num/RL_den
        del RL_num
        del RL_den
        
        return (CL, RL)
    
    def __RS_CS_circle(self, i):
        """
            Private function    
        
            Compute the Center CS and the radius RS of the source stability circle
        """
        CS_num=np.conj(self.s[i][0][0]-self.delta[i]*np.conj(self.s[i][1][1]))   
        CS_den=np.absolute(self.s[i][0][0])**2-np.absolute(self.delta[i])**2 
        CS=(CS_num/CS_den)
        del CS_num
        del CS_den
        
        RS_num=np.absolute(self.s[i][0][1]*self.s[i][1][0])
        RS_den=np.absolute(self.s[i][0][0])**2-np.absolute(self.delta[i])**2
        RS=RS_num/RS_den
        del RS_num
        del RS_den
        
        return (CS, RS)
    
    def __set_CF(self, F: float):
        """
            Private function    
        
            Compute the Center CF and the radius RF of the Noise circle for a given F.
        """
        N=((F-self.NFmin)/(4*self.rn_norm))*np.absolute(1+self.gamma_opt)**2
        CF=self.gamma_opt/(N+1)
        RF=np.sqrt(N*(N+1-np.absolute(self.gamma_opt)**2))/(N+1)             
        return (CF, RF)
    
    def plot_Unilateral_Test(self):
        """
           Plot limites de U du transistor en fonction de la fréquence 
        """   
        x=np.array([self.freq, self.freq])
        y=np.array(self.U)
        self.__plot(x, np.swapaxes(y,0,1), self.Network.frequency.unit, "dB", labels=["Umin", "Umax"])
        
    def plot_gain_transducer(self):
        """
            Plot transducer gain du transistor en fonction de la fréquence 
        """   
        x=np.array([self.freq])
        y=np.array([10*np.log10(self.GT)])
        self.__plot(x, y, self.Network.frequency.unit,"dB", labels=["Gt, Transducer Power Gain"])    
        
    def plot_gain_available(self):
        """
            Plot gain disponible (available gain) du transistor en fonction de la fréquence 
        """   
        x=np.array([self.freq])
        y=np.array([10*np.log10(self.GA)])
        self.__plot(x, y, self.Network.frequency.unit,"dB", labels=["Ga, Available Power Gain"])      
        
    def plot_power_gain(self):
        """
            Plot le gain (power gain G) du transistor en fonction de la fréquence 
        """          
        x=np.array([self.freq])
        y=np.array([10*np.log10(self.G)])
        self.__plot(x, y, self.Network.frequency.unit,"dB", labels=["G, Power gain"])              
        
    def plot_maximum_gain(self):
        """
            Plot le gain max (maximum gain G) du transistor en fonction de la fréquence 
        """   
        x=np.array([self.freq])
        y=np.array([10*np.log10(self.GMAX)])
        self.__plot(x, y, self.Network.frequency.unit,"dB", labels=["Gmax, Power Gain with Conjugate Match"])  
        
    def plot_stable_gain(self):
        """
            Plot le gain stable (maximum stable gain GS) du transistor en fonction de la fréquence 
        """   
        x=np.array([self.freq])
        y=np.array([10*np.log10(self.GS)])
        self.__plot(x, y, self.Network.frequency.unit,"dB", labels=["Gstable, Maximum stable power gain"])          
        
    def plot_gain(self):
        """
            Plot les différents gains (GT, GA, G) du transistor 
            en fonction de la fréquence sur une même figure
        """         
        x=np.array([self.freq, self.freq, self.freq, self.freq, self.freq])
        y=np.array([10*np.log10(self.GA), 10*np.log10(self.GT), 10*np.log10(self.G),
                    10*np.log10(self.GMAX), 10*np.log10(self.GS)])
        
        self.__plot(x, y, self.Network.frequency.unit,"dB", labels=["Ga, Available gain", "Gt, transducer gain", "G, Power gain", "Gmax, Power Gain with Conjugate Match", "Gstable, Maximum stable power gain"])              
        
    def plot_Sparam(self, size : tuple = (8,8), PlotStyle: str = 'seaborn-v0_8-notebook', linewidth: int | float = 2):
        """
           Affiche les modules des paramètres S du transistor en dB en focntion de la fréquence
        """
        with plt.style.context(PlotStyle): 
            fig = plt.figure(figsize=size)
            ax1 = fig.add_subplot(221) 
            ax2 = fig.add_subplot(222) 
            ax3 = fig.add_subplot(223) 
            ax4 = fig.add_subplot(224) 
            self.Network.plot_s_db(m=0,n=0,ax=ax1,linewidth=linewidth,linestyle='-',label='|S11|',color="tab:olive") 
            self.Network.plot_s_db(m=0,n=1,ax=ax2,linewidth=linewidth,linestyle='-',label='|S12|',color="tab:blue") 
            self.Network.plot_s_db(m=1,n=0,ax=ax3,linewidth=linewidth,linestyle='-',label='|S21|',color="tab:red") 
            self.Network.plot_s_db(m=1,n=1,ax=ax4,linewidth=linewidth,linestyle='-',label='|S22|',color="tab:orange") 
           
    def __plot(self, x, y, 
               x_unit: str="GHz", y_unit: str="", 
               labels:str=[""], 
               size : tuple = (8,4.5), 
               PlotStyle: str = 'seaborn-whitegrid', 
               linewidth: int | float = 2): 
        
        """
            Fonction privée 
            
            Plot X versus Y
        """   
        
        with plt.style.context(PlotStyle): 
            fig = plt.figure(figsize=size)
            ax1 = fig.add_subplot(111)            
            l,c=y.shape
            for i in range(l):
                plt.plot(x[i], y[i], label=labels[i], linewidth=linewidth)
            plt.xlabel("Frequency ("+x_unit+")")
            plt.ylabel(y_unit)

            if labels !="":
                plt.legend()
                
    def __plot2(self, x1, y1, x2, y2,
                x_unit: str="GHz", y_unit:str="",
                labels1:str="", labels2:str="",
                size : tuple = (8,4.5), 
                PlotStyle: str = 'seaborn-whitegrid', 
                linewidth: int | float = 2):   
        
        """
            Fonction privée 
            
            Plot 2 vecteurs dans une même fenêtre
        """  
        
        with plt.style.context(PlotStyle): 
            fig, (ax1, ax2) = plt.subplots(1, 2, figsize=size)
            for i in range(len(y1.shape)):
                ax1.plot(x1[i], y1[i], label=labels1[i], linewidth=linewidth)
            ax1.set(xlabel="Frequency ("+x_unit+")", ylabel=y_unit) 
            ax1.legend()
            
            for i in range(len(y2.shape)): 
                ax2.plot(x2[i], y2[i], label=labels2[i], linewidth=linewidth)
            ax2.set(xlabel="Frequency ("+x_unit+")", ylabel=y_unit)
            ax2.legend()
  
    def plot_rollet(self):
        """
           Plot le facteur de Rollet du transistor en fonction de la fréquence 
        """   
        x=np.array([self.freq, self.freq])
        y=np.array([self.K, np.absolute(self.delta)])
        self.__plot(x, y, self.Network.frequency.unit,labels=["K","|\u0394|"])
                    
    def plot_mu_stab(self):
        """
           Plot the µ stability factors 
        """   
        x=np.array([self.freq, self.freq])
        y=np.array(self.mu_stab)
        self.__plot(x, np.swapaxes(y,0,1), self.Network.frequency.unit, labels=["µ Source","µ Load"])    
    
    def stability(self):
        """
           Check the stability factors and plot stability parameters
        """   
        # Check of the stability:
        for Ki in self.K:
            if Ki <1:
                print("The transistor is not inconditionally stable")
                break
        
        # Plot stability parameters:
        x1=np.array([self.freq, self.freq])
        x2=x1
        y1=np.array([self.K, np.absolute(self.delta)])
        y2=np.swapaxes(np.array(self.mu_stab),0,1)
        self.__plot2(x1,y1,x2,y2, self.Network.frequency.unit, labels1=["K","|\u0394|"], 
                     labels2=["µ Source","µ Load"]) 
        
    def stability_circle(self, points: int=100, 
                         size : tuple = (8,8), 
                         PlotStyle: str = 'seaborn-whitegrid', 
                         linewidth: int | float = 2):
        """
            Plot the stability circle, Source and Load
            on a Smith Chart.
            
            Stability circles are computed at one frequency, f0, given by the user, 
            or by default equal to the first frequency point in the rf.Network
            
        """
        print(f"Enter the frequency at which the stability circle is plotted (in {self.Network.frequency.unit}):")
        print("By default the first frequency point is chosen.")
        f0=float(input("frequency?") or f"{self.Network.frequency.f_scaled[0]}")

        # find the index value for the frequency f0
        i=self.freq.searchsorted(f0)
        
        # Compute the center and radius    
        CL, RL= self.__RL_CL_circle(i)
        CS, RS= self.__RS_CS_circle(i)
        
        Circles=np.zeros((points,2), dtype=complex)
        theta=np.linspace(0, 2*np.pi, points)
        
        # Compute the vector of circles
        for n in range(points):
            Circles[n][0]=CS+RS*complex(real=np.cos(theta[n]), imag=np.sin(theta[n]))
            Circles[n][1]=CL+RL*complex(real=np.cos(theta[n]), imag=np.sin(theta[n]))
             
        # Determination of stability zone    
        if np.absolute(self.s[i][0][0]) < 1:
            if np.absolute(CS)-np.absolute(RS)<0:
                print('--> Source stability region is inside of the circle')
                Source_zone="inside"
            else:
                print('--> Source stability region is outside of the circle')
                Source_zone="outside"
        else:
            if np.absolute(CS)-np.absolute(RS)<0:
                print('--> Source stability region is outside of the circle')
                Source_zone="outside"
            else:
                print('--> Source stability region is inside of the circle')
                Source_zone="inside"                       
            
        if np.absolute(self.s[i][1][1]) < 1:
            if np.absolute(CL)-np.absolute(RL)<0:
                print('--> Load stability region is inside of the circle')
                Load_zone="inside"
            else:
                print('--> Load stability region is outside of the circle')
                Load_zone="outside"
            
        else:
            if np.absolute(CL)-np.absolute(RL)<0:

                print('--> Load stability region is outside of the circle')
                Load_zone="outside"
            else:
                print('--> Load stability region is inside of the circle')
                Load_zone="inside"        
        #Plot
        CS_nt=rf.Network(freq=f0, s=Circles[:, 0], name='Source Stability circle (Stability region is '+Source_zone+' the circle)')
        CL_nt=rf.Network(freq=f0, s=Circles[:, 1], name='Load Stability circle (Stability region is '+Load_zone+' the circle)')
        
        with plt.style.context(PlotStyle): 
            fig = plt.figure(figsize=size)
            CS_nt.plot_s_smith(marker='+', linewidth=linewidth, label=CS_nt.name)
            CL_nt.plot_s_smith(marker='>', linewidth=linewidth, label=CL_nt.name)       
            plt.title(f"Stability circles at {f0} {self.Network.frequency.unit}")
        
    def noise_circle(self, points: int=100, 
                         size : tuple = (8,8), 
                         PlotStyle: str = 'seaborn-whitegrid', 
                         linewidth: int | float = 2):
        
        f0="-1"
        
        while (eval(f0) < self.Network.f_noise.f_scaled[0]) or (eval(f0) > self.Network.f_noise.f_scaled[-1]):
            print(f"Enter the frequency for the noise circle (in {self.Network.frequency.unit}):\n")
            f0=str(input(f"frequency? (Default value is {self.Network.f_noise.f_scaled[0]} {self.Network.frequency.unit})") or f"{self.Network.f_noise.f_scaled[0]}")
        
            if eval(f0) < self.Network.f_noise.f_scaled[0]:
                print(f"\n /!\ the frequency has to be superior to {self.Network.f_noise.f_scaled[0]} {self.Network.frequency.unit}\n")
           
            if eval(f0) > self.Network.f_noise.f_scaled[-1]:
                print(f"\n /!\ the frequency has to be inferior to {self.Network.f_noise.f_scaled[-1]} {self.Network.frequency.unit}\n")       
        
        self.rn_norm = self.Network[f"{eval(f0) }"+self.Network.frequency.unit].rn[0]/50
        self.NFmin = np.power(10, self.Network[f"{eval(f0) }" +self.Network.frequency.unit].nfmin[0]/10)
        self.gamma_opt = self.Network[f"{eval(f0) }" +self.Network.frequency.unit].g_opt[0] 
        
        print(f"\n The minimum Noise Factor is {10*np.log10(self.NFmin):0.2f} dB")
        
        print(f"Enter the Noise Factor for the stability circle (in dB)")
        Fmin_dB=str(input("NF min? (the default value is Fmin)") or f"{10*np.log10(self.NFmin)}")
        Fmax_dB=str(input("NF max? (the default value is Fmin)") or f"{10*np.log10(self.NFmin)}")
        Fn=eval(input("nombre de cercles? (The default value is 1)") or "1")
        
        Fmin=np.power(10, eval(Fmin_dB)/10)
        Fmax=np.power(10, eval(Fmax_dB)/10)

        Circles=np.zeros((points, Fn), dtype=complex)
        theta=np.linspace(0, 2*np.pi, points)
        Fvec=np.linspace(Fmin, Fmax, Fn)
        CNF_dico={}
        CF_dico={}
        
        # Compute the vector of circles
        for i, F in enumerate(Fvec):
            CF, RF=self.__set_CF(F)
            for j in range(points):
                Circles[j][i]=CF+RF*complex(real=np.cos(theta[j]), imag=np.sin(theta[j]))         
            CNF_nt=rf.Network(freq=eval(f0), s=Circles[:, i], name=f"Noise circles F={10*np.log10(F):0.2f} dB")
            CF_nt=rf.Network(freq=eval(f0), s=CF, name=f"Noise circles Center for F={10*np.log10(F):0.2f} dB")
            CNF_dico[f"F={10*np.log10(F):0.2f} dB"] = CNF_nt
            CF_dico[f"F={10*np.log10(F):0.2f} dB"] = CF_nt
        CNF_nts = rf.NetworkSet(CNF_dico, name='NF circle set')
        CF_nts = rf.NetworkSet(CF_dico, name='NF center circle set')
        #Plot
        with plt.style.context(PlotStyle): 
            fig = plt.figure(figsize=size)
            for i in range(Fn):
                CNF_nts[i].plot_s_smith(marker='+', linewidth=linewidth, label=CNF_nts[i].name)          
                CF_nts[i].plot_s_smith(linestyle='', marker='+', linewidth=linewidth, label=CF_nts[i].name)
            plt.title(f"Noise circle at {f0} {self.Network.frequency.unit}")
                

            
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          